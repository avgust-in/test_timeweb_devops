FROM node:16    

EXPOSE 3333
WORKDIR /app
COPY . /app

RUN  yarn
RUN yarn build

CMD yarn start
